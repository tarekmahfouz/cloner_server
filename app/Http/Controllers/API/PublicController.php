<?php

namespace App\Http\Controllers\API;

use Carbon\Carbon;
use App\Http\Resources\User as UserResource;
use Illuminate\Http\Request;

class PublicController extends InitController
{
    public function retrieve(Request $request)
    {
        $code = 200;
        $message = 'done.';
        $data = [];
        try {
            $limit = $request->limit ?? 10;

            $conditions = [];
            if($request->filled('phone')) {
                $user = $this->serviceObj->find('User', ['phone' => $request->phone]);
                $conditions[] = ['id','>',$user->id];
            }
            $list = $this->serviceObj->getAll('User', $conditions, $limit);
            $data = UserResource::collection($list);
            //Storage::disk('public')->put('users.json', json_encode($data));
        } catch (\Throwable $th) {
            $code = getCode($th->getCode());
            $message = $th->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }
}

