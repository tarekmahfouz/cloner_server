<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Services\CommonService;
use Illuminate\Http\Request;
use Carbon\Carbon;

class InitController extends Controller
{
    public $serviceObj;
    
    public function __construct()
    {
        app()->setLocale('ar');
        Carbon::setLocale('en');
        
        $this->middleware(function($request, $next) {
            $this->serviceObj = new CommonService;
            
            return $next($request);
        });
    }
}
