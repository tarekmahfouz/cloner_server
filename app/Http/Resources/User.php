<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            //'id' => $this->id,
            'phone' => $this->phone,
            'date' => $this->date->format('Y-m-d'),
            'time' => $this->time->format('H:i:s'),
        ];
    }
}
